#!/bin/bash

a='author'
f='slide'

# build
latexmk -pdf -lualatex $f.tex
#makeglossaries $f
#rm $f.pdf # trick remove to rebuild
#latexmk -pdf -lualatex $f.tex


# clean
arg=${1:-.}
exts="acn aux bbl bcf blg blx.bib brf fdb_latexmk fls idx ilg ind ist lof log lol lot nav out run.xml snm toc synctex.gz acr alg glg glo gls glsdefs loa maf mtc mtc0"

if [ -d $arg ]; then
    for ext in $exts; do
         rm -f $arg/*.$ext
	 rm -f $arg/*-$ext
    done
else
    for ext in $exts; do
         rm -f $arg.$ext
    done
fi

# rename
# mv $f.pdf $a\_$f\_$(date +%Y%m%d).pdf
