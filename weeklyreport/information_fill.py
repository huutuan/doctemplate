#!/usr/bin/python
# -*- coding: utf-8 -*-

# Hướng dẫn: 
# 	Tạo một object Filler, sau đó fill đủ slot như dưới hàm __main__ rồi chạy method obj.tofill => done 
# Lưu ý: 
# 	1) Truyền input_path và output_path 
# 	2) Có week và year sẽ tính được datefrom và dateto
#   3) Mảng tex_array phải có shape là (5*3) 

import os
import argparse 
import numpy as np
import datetime
import re
from utils.handle import Time_handler_workingday, refac_time, parser, get_week_number
from utils.handle import daterange, latex_preprocessing, remove_tone_notation, convert, generate_eid_from_name


import db_api

# time = datetime.datetime.now()

input_path = './template.tex'
output_path = './weeklyreport.tex'

def args():
	parser = argparse.ArgumentParser(description='Process some integers.')
	parser.add_argument("--input", help="input path", type=str)
	parser.add_argument("--output", help="output path", type=str)
	args = parser.parse_args()
	return args 

class Filler():
	def __init__(self):
		self.week = None		# <week> 
		self.year = None	    # <year>
		self.datefrom = None	    # <datefrom>
		self.dateto = None	    # <dateto> 
		self.name = None		# <name>
		self.manager = None	    # <manager>
		self.position = None		# <position>
		self.group = None		# <group>
		self.tex_array = np.zeros((5,3), dtype= str)	# <tex_array>

	def tofill(self, input_path, output_path):
		data = {}
		if self.week is not None:
			data['<week>'] = self.week
			data['<year>'] = time.year
			first_time, last_time = Time_handler_workingday(time.year, self.week)
			self.datefrom = refac_time(first_time)
			self.dateto = refac_time(last_time)
			data['<datefrom>'] = self.datefrom
			data['<dateto>'] = self.dateto
		# if self.year is not None:
		# 	data['<year>'] = self.year
		# if self.datefrom is not None:
		# 	data['<datefrom>'] = self.datefrom
		# if self.dateto is not None:
		# 	data['<dateto>'] = self.dateto
		if self.name is not None:
			data['<name>'] = self.name
		if self.manager is not None:
			data['<manager>'] = self.manager
		if self.position is not None:
			data['<position>'] = self.position
		if self.group is not None:
			data['<group>'] = self.group
		data['tex_array'] = self.tex_array
		print(data)

		#fill
		parser(data, input_path, output_path)
		return []

if __name__ == '__main__':
	# arg = args()
	# input_path = arg.input
	# output_path = arg.output

	time = datetime.datetime.now()
	idslacks = db_api.get_all_employees()
	current_year = time.year 
	current_week = get_week_number(time) - 1

	for slack_id in idslacks:
		workingdays = [str(date) for date in daterange(Time_handler_workingday(current_year, current_week)[0], Time_handler_workingday(current_year, current_week)[1])]
		workingdays = [str(date) for date in daterange(Time_handler_workingday(2019, 39)[0], Time_handler_workingday(2019, 39)[1])]
		log = db_api.get_logs_from_db(slack_id, workingdays)
		if log is None:
			continue
		workinglog = log['logs']

		reporttable = []
		for d in workingdays:
			if d in workinglog:
				reporttable += [[latex_preprocessing(workinglog[d]['pre-day']['message']),
				latex_preprocessing(workinglog[d]['current-day']['message']), 
				latex_preprocessing(workinglog[d]['challenge']['message'])
				]]
			else:
				reporttable += [['no data', 'no data', 'no data']]

		userinfo = db_api.get_user_name_by_id(slack_id)
		eid = generate_eid_from_name(userinfo["FirstName"], userinfo["MiddleName"], userinfo["LastName"])
		# khi thêm vào script trên rasa thì cmt dòng trên, bỏ cmt 2 dòng dưới để thống nhất tên eid 
		# from db_api.db_employee_api import get_user_name_by_id
		# eid = get_user_name_by_id(slack_id)

		obj = Filler()
		obj.year = current_year
		obj.week = current_week
		obj.name = userinfo['Name']
		obj.position = userinfo['Status']
		obj.tex_array = reporttable
		obj.tofill(input_path, output_path)
		
		cmd = 'bash build_release.sh ' + str(obj.year) + ' ' + str(obj.week) + ' ' + eid
		print(cmd)
		# os.system(cmd)
		
	# for fi in os.listdir():
	# 	if os.path.splitext(fi)[1] == '.pdf':
	# 		print(fi)
	# 		cmd = 'bash upload-gdrive.sh ' + fi
	# 		os.system(cmd)
	# 		cmd = 'rm ' + fi
	# 		os.system(cmd)