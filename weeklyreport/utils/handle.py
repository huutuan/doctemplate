import datetime 
import re


patterns = {
    '[àáảãạăắằẵặẳâầấậẫẩ]': 'a',
    '[đ]': 'd',
    '[èéẻẽẹêềếểễệ]': 'e',
    '[ìíỉĩị]': 'i',
    '[òóỏõọôồốổỗộơờớởỡợ]': 'o',
    '[ùúủũụưừứửữự]': 'u',
    '[ỳýỷỹỵ]': 'y'
}


def Time_handler_workingday(p_year, p_week):
	"""
		get day of week (first and last)
	"""
	firstdayofweek = datetime.datetime.strptime(f'{p_year}-W{int(p_week )- 1}-1', "%Y-W%W-%w").date()
	lastdayofweek = firstdayofweek + datetime.timedelta(days=4.9)
	firstdayofweek.strftime('%A')
	lastdayofweek.strftime('%A')

	return firstdayofweek, lastdayofweek

# firstdate, lastdate =  Time_handler_workingday('2019','2')
# print('result ',firstdate,' ', lastdate)

def get_week_number(time):	#time.year / time.month / time.day
	return int(datetime.date(time.year, time.month, time.day).isocalendar()[1])
# time = datetime.datetime.now()
# print(get_week_number(time))

def refac_time(tim):
	"""
		convert to format 26-11-2019 
	"""
	return str(tim.day)+'-'+str(tim.month)+'-'+str(tim.year)


def parser(data, in_path, out_path):	#data : dict
	"""
		data need to replace 
		in_path is path of input latex file 
		out_path is path of output latex file
	"""
	with open(in_path, 'r') as f:
		reader = f.readlines()

	for key in data:
		texdoc = []
		for line in reader:
			texdoc.append(line.replace(key, str(data[key])))
		reader = texdoc

	##fill array 
	tex_array = data['tex_array']
	texdoc = []
	for i in range(5):
		for j in range(3):
			for line in reader:
				name = "<text"+str(i+1)+str(j+1)+">"
				texdoc.append(line.replace(name, str(tex_array[i][j]) ))
			reader = texdoc
			texdoc = []
		print(name)

	with open(out_path, 'w') as f:
		for line in reader:
			f.write(line)

	return []



#######  
def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days) + 1):
        yield start_date + datetime.timedelta(n)

def latex_preprocessing(text):
	return text.replace("_", "\_")

def remove_tone_notation(text):
    """
    Remove Vietnamese tone notation
    text: input string to be converted
    Return: converted string which is removed ton notation
    """
    output = text
    for regex, replace in patterns.items():
        output = re.sub(regex, replace, output)
        # deal with upper case
        output = re.sub(regex.upper(), replace.upper(), output)
    return output

def convert(s): 
  
    # initialization of string to "" 
    new = "" 
  
    # traverse in the string  
    for x in s: 
        new += x  
  
    # return string  
    return new 

def generate_eid_from_name(first, middle, last):
	_first = remove_tone_notation(first).lower()
	_middle = remove_tone_notation(middle).lower()
	_last = remove_tone_notation(last).lower()

	eid = _first + ''.join([a[0] for a in _last.split()]) + ''.join([a[0] for a in _middle.split()])
	return eid 