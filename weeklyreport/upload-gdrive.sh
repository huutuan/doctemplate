#!/bin/sh
echo $REFRESH_TOKEN
echo $CLIENT_ID
echo $CLIENT_SECRET

FILENAME=$1
FOLDER_ID="1Tk0V62o3rdorunfdNuJEibUt19v-Cvel"

# refresh token
echo "Refresh access token"
output=$(curl -s -H 'Content-Type: application/x-www-form-urlencoded' \
    -X POST "https://www.googleapis.com/oauth2/v4/token" \
    -d "client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&refresh_token=$REFRESH_TOKEN&grant_type=refresh_token")

ACCESS_TOKEN=$(echo $output | jq -r .access_token)
echo "Access token: ${ACCESS_TOKEN:0:10}..."


for FILENAME in "$@"
do
    echo "Upload $FILENAME"
    # upload file
    curl -s -X POST -H "Authorization: Bearer $ACCESS_TOKEN" \
        -F "metadata={name : \"$(basename $FILENAME)\", parents: [\"$FOLDER_ID\"]};type=application/json;charset=UTF-8" \
        -F "file=@$FILENAME;type=application/pdf" \
        "https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart"
done
