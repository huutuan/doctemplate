from pymongo import MongoClient
import pymongo
import requests
import pandas as pd
import configparser
from slackclient import SlackClient

config = configparser.ConfigParser()
config.read('./config.ini')

# Load config variable
host = config['MONGODB_CONNECTION']['HOST']
port = int(config['MONGODB_CONNECTION']['PORT'])
username = config['MONGODB_CONNECTION']['USERNAME']
password = config['MONGODB_CONNECTION']['PASSWORD']
slack_token = config['SLACK']['SLACK_TOKEN']

slack_client = SlackClient(token=slack_token)


# Connect to server Mongo
def connect_to_mongo_db():
    try:
        return MongoClient(host=host, port=port, username=username, password=password)
    except pymongo.errors.ServerSelectionTimeoutError as err:
        return None


client = connect_to_mongo_db()

db = client.tizzie_user

def append_user(user):
    '''
    add one user to mongodb
    :return: True if succeed, otherwise False
    '''

    collection = db.users
    collection.insert_one(user)

    return True

def get_all_employees():

    collection = db.employees_list
    items = collection.find({})
    iitems = []
    for item in items:
        iitems.append(item["IdSlack"])
    idslacks = [x for x in iitems if str(x) != 'nan']

    return idslacks

def init_db_users():
    '''
    init
    :return: True if succeed
    '''
    del_all_users()
    request = slack_client.api_call("users.list")
    # pprint(request)
    users = []
    if request['ok']:
        for item in request['members']:
            append_user(item)

    return True

def del_all_users():
    '''
    delete all users from mongodb
    :return: True if succeed, otherwise False
    '''
    collection = db.users
    collection.delete_many({})

    return True

def get_info_user(id=None, name=None):
    '''
    Get info user by id or name
    :param id: id of user
    :param name: name of user
    :return: Dict depite info user in slack
    '''

    return None

def get_user_name_by_id(id):
    pipeline = [
    {'$match': {'IdSlack': id}},
    {'$project': {
        'FirstName': 1,
        'MiddleName': 1,
        'LastName': 1,
        'Email': 1,
        'Status': 1,
    }}]
    items = db.employees_list.aggregate(pipeline)
    iitems = []
    for item in items:
        iitems.append(item)
        tmp = {}
    tmp["Name"] = iitems[0]['LastName'] + " " + iitems[0]['MiddleName'] + " " + iitems[0]['FirstName']
    tmp["Email"] = iitems[0]["Email"]
    tmp["Status"] = iitems[0]["Status"]
    tmp["FirstName"] = iitems[0]["FirstName"]
    tmp["MiddleName"] = iitems[0]["MiddleName"]
    tmp["LastName"] = iitems[0]["LastName"]
    return tmp

def get_all_user_in_slack():

    pipeline = [
        {'$project':{
            'idslack': 1
        }}
    ]
    items = db.items.aggregate(pipeline)
    iitems = []
    for item in items:
        iitems.append(item)

    return iitems

def dell_collection_logs():

    collection = db.logs
    collection.delete_many({})

    return True

def init_db_logs():

    dell_collection_logs()
    idslacks = get_all_employees()
    for id in idslacks:
        tmps = {'idslack': id, 'logs': []}
        collection = db.logs
        collection.insert_one(tmps)


def append_logs_into_db(idslack, mess1, mess2, mess3, today):

    day = str(today)
    db.logs.update(
        {'idslack': idslack},
        {
            '$push':{
                'logs': {
                    day :{
                        "pre-day": {"message": mess1[0], "timestamp": mess1[1]},
                        "current-day": {"message": mess2[0], "timestamp": mess2[1]},
                        "challenge": {"message": mess3[0], "timestamp": mess3[1]}
                    }
                }
            }
        }
    )

    return None

def get_logs_from_db(idslack, datestr_list):
    re = db.logs.aggregate([
            {"$match": {"idslack": idslack } },
            {"$unwind": "$logs"},
            {"$project": { "logs2": { "$objectToArray" : "$logs" } } },
            {"$match": {"logs2.k" : {"$in" : datestr_list } }},
            {"$unwind": "$logs2"}, 
            {"$group" : {"_id": None, "logs": {"$push": "$logs2" } } },
            {"$project": {"logs" : {"$arrayToObject" : "$logs"} } }  
            ])
    # lst = [item for item in re]
    return next(re, None)


def init_db_employees_list(path):

    '''
    Khởi tạo db danh sách những nhân viên có trong công ty tại thời điểm publish bot
    '''

    df = pd.read_csv(path, sep=',', header=0)

    for i in range(len(df)):
        tmp = {}
        tmp["IdSlack"] = df.iloc[i]["IdSlack"]
        tmp["LastName"] = df.iloc[i]["LastName"]
        tmp["MiddleName"] = df.iloc[i]["MiddleName"]
        tmp["FirstName"] = df.iloc[i]["FirstName"]
        tmp["Status"] = df.iloc[i]["Status"]
        tmp["Email"] = df.iloc[i]["Email"]
        tmp["University"] = df.iloc[i]["University"]

        collection = db.employees_list
        collection.insert_one(tmp)


def add_new_employee(idslack=None, lastname=None, middlename=None, firstname=None, status=None, email=None, university=None):
    '''
    Thực thi việc thêm một nhân viên mới để có thể lấy được logs chat của user đó.
    > Thêm vào db employees_list và db logs
    '''

    tmp = {}
    tmp["IdSlack"] = idslack
    tmp["LastName"] = lastname
    tmp["MiddleName"] = middlename
    tmp["FirstName"] = firstname
    tmp["Status"] = status
    tmp["Email"] = email
    tmp["University"] = university

    collection1 = db.employees_list
    collection1.insert_one(tmp)

    ret = {'idslack': idslack, 'logs': []}

    collection2 = db.logs
    collection2.insert_one(ret)