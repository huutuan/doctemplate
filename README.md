[![pipeline status](https://gitlab.ftech.ai/nlp/doctemplate/badges/master/pipeline.svg)](https://gitlab.ftech.ai/nlp/doctemplate/commits/master)

# DocTemplate

Template for internal use documents of NLP team

Article template preview [download here](https://gitlab.ftech.ai/nlp/doctemplate/builds/artifacts/master/file/article.pdf?job=build)

Slide template preview [download here](https://gitlab.ftech.ai/nlp/doctemplate/builds/artifacts/master/file/slide.pdf?job=build)

Report template preview [download here](https://gitlab.ftech.ai/nlp/doctemplate/builds/artifacts/master/file/report.pdf?job=build)

Employee weekly report template preview [download here](https://gitlab.ftech.ai/nlp/doctemplate/builds/artifacts/master/file/weeklyreport.pdf?job=build)


## Latex installation for Ubuntu
1. Tex compiler
```bash
apt install texlive-full
```

2. Tex editor (recommend Sublime Text)
 - Install LaTeXTools package
 - Using LaTeXTools to check missing packages (install them if missing)

3. Build release
- Change directory to your project report
```bash
bash build_release.sh
```

4. Gitlab CI
 - In case you need to change report file name, you must change the variable in build script matching with the artifact path in gitlab-ci.yml
 - By default the name of output PDF file is report.pdf
